;;; lookup.el --- Easily look things up              -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Case Duckworth

;; Author: Case Duckworth <acdw@acdw.net>
;; Keywords: docs, help, hypermedia

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Emacs has incredible documentation functionality for elisp:
;; `describe-function', `describe-variable', and that whole family, and the
;; family of `find-function', `find-library', and more.  The describe- family of
;; functions is even helpfully bound to <C-h ...>, and Emacs's
;; self-documentation makes it incredibly easy to use these all the time.

;; HOWEVER, this muscle-memory for the C-h binds does not translate to other
;; major modes.  In many of those modes, users are left using C-c C-d or other
;; binds to find documentation about those langauages' functionalities.

;; This package proposes a new method of documentation lookup that's
;; mode-dependent.  When enabling the global minor mode `lookup-mode', the
;; bindings under the <help> key are changed in relevant major modes to perform
;; that language's lookup function instead.  Of course, since we are /in/ Emacs,
;; having the describe- family of functions available everywhere is a good
;; idea---so they're under <help> <help>.

;; (Idea from [[https://old.reddit.com/r/emacs/comments/uee7ka/ch_f_equivalent_in_other_languages/][a reddit post]])

;;; Code:

(require 'thingatpt)

;;; Customization options

(defcustom lookup-alist
  (let* ((helpfulp (require 'helpful nil :noerror))
         (function-fn (if helpfulp #'helpful-callable #'describe-function))
         (variable-fn (if helpfulp #'helpful-variable #'describe-variable))
         (at-point-fn (if helpfulp #'helpful-at-point #'lookup--describe-symbol-at-point)))
    `((function . ((t . ,function-fn)))
      (variable . ((t . ,variable-fn)))
      (at-point . ((t . ,at-point-fn)))))
  "Alist of alists defining how to look up various things.
The alist is of the form ((TYPE . ((MODE . FUNCTION)...))...),
where TYPE is the type of symbol to look up, and FUNCTION is the
function to use for lookup in MODE.  MODE can be a list of modes;
it's passed to `derived-mode-p'.  The order of MODEs and
FUNCTIONs matters.

Finally, a FUNCTION corresponding to a MODE of t will serve as a
fallback.  By default, these are defined as `helpful' functions
if the library `helpful' is installed, otherwise
`describe-function' and the similar.")

;;; Functionality

(defun lookup--describe-symbol-at-point ()
  "Attempt to describe the symbol at point."
  (interactive)
  (describe-symbol (intern (thing-at-point 'symbol t))))

(defun lookup--thing (type symbol)
  "Lookup a SYMBOL of type TYPE.
TYPE should be one of the cars of `lookup-alist', which see."
  (let* ((type-alist (alist-get type lookup-alist))
         (fn (catch :found
              (dolist (cell type-alist)
                  (when (apply #'derived-mode-p (car cell))
                    (throw :found (cdr cell))))
              (alist-get t type-alist))))
    (funcall fn symbol))) ;;; XXX: some functions take strings, some symbols ....

;; THESE ARE STUBS

(defun lookup-function (symbol)
  ;; TODO: This is a stub --- I need an `interactive'  form that allows
  ;; completing-read and has the function at point as the default value.
  (lookup--thing 'function symbol))

(defun lookup-variable (symbol)
  (lookup--thing 'variable symbol))

(defun lookup-at-point ()
  (lookup--thing 'at-point))

;;; Minor mode

(defun lookup--bind-help-keys (map key command)
  "Bind KEY to COMMAND under all the help keys in MAP."
  (let ((c-h-k (kbd (concat "C-h " key)))
    (f1-k (kbd (concat "<f1> " key)))
    (help-k (kbd (concat "<help> " key))))
    (define-key map c-h-k command)
    (define-key map f1-k command)
    (define-key map help-k command)))

(define-minor-mode lookup-global-mode
  "Look up symbols depending on current mode."
  :lighter " f1@"
  ;; XXX: This doesn't work
  :keymap (let ((map (make-sparse-keymap)))
            (lookup--bind-help-keys map "f" #'lookup-function)
            ;; `help-command' is still reachabel with a double tap
            (lookup--bind-help-keys map "<f1>" #'help-command)
            (lookup--bind-help-keys map (char-to-string help-char) #'help-command)
            (lookup--bind-help-keys map "<help>" #'help-command)
            map)
  :global t)

(provide 'lookup)
;;; lookup.el ends here
